<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Controller;
use \App\Http\Controllers\HomeController;
use App\Mail\MessagesMail;
use App\Http\Controllers\FeedbackController;

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ], function(){
    Route::get('/', [Controller::class, 'index'])->name("main.page");
    Route::get('/products', [Controller::class, 'products'])->name("main.products");
//    Route::get('/about', [Controller::class, 'about'])->name("main.about");
    Route::get('/contacts', [Controller::class, 'contacts'])->name("main.contacts");
    Route::get('/target_product/{product}', [Controller::class, 'target_product'])->name('target.product');

    //THIS WILL BE USED WHEN WE IMPLEMENT PHP MAILER
    Route::get('/send', [FeedbackController::class, 'send'])->name("main.send.message");

    Auth::routes();

    Route::get('home', [HomeController::class, 'index'])->middleware("auth")->name('home');
    Route::prefix("admin")->middleware("auth")->group(function () {

        Route::get('add_item', [HomeController::class, 'add_item_form'])->name('add.form');

        Route::post('add_action', [HomeController::class, 'add_action'])->name('add.action');

        Route::get('redact_item', [HomeController::class, 'redact_item'])->name('redact.item');

        Route::delete('delete_item/{item}', [HomeController::class, 'delete_item'])->name("delete.item");

        Route::get('update_item_form/{item}', [HomeController::class, 'update_item_form'])->name("update.item");

        Route::post('redact_action', [HomeController::class, 'redact_action'])->name("redact.action");
    });
});
