<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MessagesMail extends Mailable
{
    use Queueable, SerializesModels;

    public $emails;

    public function __construct($email)
    {
        $this->emails = $email;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Message from Visitor')
            ->view('mail')
            ->with('emails', $this->emails);
    }

}


