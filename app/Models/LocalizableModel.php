<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

abstract class LocalizableModel extends Model
{
    use HasFactory;
    protected $localizable = [];
    /**
     * Magic method for retrieving a missing attribute.
     *
     * @param string $attribute
     * @return mixed
     */
    public function __get($attribute)
    {
        if (in_array($attribute, $this->localizable)) {
            $localeSpecificAttribute = $attribute.'_'.LaravelLocalization::getCurrentLocale();
            return $this->{$localeSpecificAttribute};
        }
        return parent::__get($attribute);
    }
}
