<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends LocalizableModel
{
    use HasFactory;

    protected $localizable = [
        "name","description"
    ];

    protected $guarded = [];
}
