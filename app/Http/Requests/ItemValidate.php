<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_hy' => 'required',
            'name_ru' => 'required',
            'price' => 'sometimes',
            'description_hy' => 'required',
            'description_ru' => 'required',
            'brand' => 'required',
            "image_path" => "sometimes|mimes:png,jpg,jfif,jpeg,svg",
        ];
    }
}
