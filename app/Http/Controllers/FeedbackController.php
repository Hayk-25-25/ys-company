<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Mail\MessagesMail;


class FeedbackController extends Controller
{
    public function send(Request $request) {
        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'message' => 'required',
        ]);

        $toEmail = "ayk.khachatryan.1994@gmail.com";
        Mail::to($toEmail)->send(new MessagesMail(['email'=>$request->email, 'message'=>$request->message]));
        return view('contacts')->with('success', 'message send');
    }
}
