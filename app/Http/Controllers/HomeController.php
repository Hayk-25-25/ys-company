<?php

namespace App\Http\Controllers;

use App\Http\Requests\ItemValidate;
use App\Models\Items;
use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.home');
    }

    public function add_item_form() {
        return view('admin.add_item_form');
    }

    public function add_action(ItemValidate $request) {

        if ($request->hasFile("image")) {
            $mainImage = $request->file('image')->getClientOriginalName();
            $file = $request->file('image');
            $file->move(base_path('./public/items_images'), $file->getClientOriginalName());
        } else {
            $mainImage = null;
        }

        Product::create([
            "name_hy" => $request->name_hy,
            "name_ru" => $request->name_ru,
            "price" => $request->price,
            "description_hy" => $request->description_hy,
            "description_ru" => $request->description_ru,
            "image_path" => $mainImage,
            "brand" => $request->category,
        ]);

        return redirect()->route("add.form")->with('success', 'Товар создан');
    }

    public function redact_item() {
        return view('admin.items_table', ['items' => Product::simplePaginate(5)]);
    }

    public function delete_item(Product $item) {
        $item->delete();

        return redirect()->back();
    }

    public function update_item_form(Product $item) {
        return view('admin.update_item_form', ['item' => $item]);
    }

    public function redact_action(ItemValidate $request) {

        $mainImage = Product::select('image_path')->where('id', $request->id)->first();
        $mainImage = $mainImage->image_path;
        if ($request->hasFile("image")) {
            $mainImage = $request->file('image')->getClientOriginalName();
            $file = $request->file('image');
            $file->move(base_path('./public/items_images'), $file->getClientOriginalName());
        }
        Product::where('id', $request->id)
            ->update([
                "name_hy" => $request->name_hy,
                "name_ru" => $request->name_ru,
                "price" => $request->price,
                "description_hy" => $request->description_hy,
                "description_ru" => $request->description_hy,
                "image_path" => $mainImage,
                "brand" => $request->category,
            ]);
        return redirect()->back()->with('success', 'товар обнавлен');
    }
}
