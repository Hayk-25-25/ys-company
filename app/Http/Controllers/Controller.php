<?php

namespace App\Http\Controllers;

use App\Models\Product;
//use http\Env\Request;
use \Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index() {
        $tris_random = Product::where('brand', '=', 'tris')->limit(3)->get();
        $crafor_random = Product::where('brand', '=', 'krafor')->limit(3)->get();
        $master_random = Product::where('brand', '=', 'masterKlein')->limit(3)->get();
        return view('welcome', compact('tris_random', 'crafor_random', 'master_random'));
    }

    public function products(Request $brand) {
        if($brand->brand == null) {
            $products = Product::Paginate(9);
        } elseif ($brand->brand == 'tris') {
            $products = Product::where('brand', '=', 'tris')->Paginate(9);
        } elseif ($brand->brand == 'krafor') {
            $products = Product::where('brand', '=', 'krafor')->Paginate(9);
        } elseif($brand->brand == 'masterKlein') {
            $products = Product::where('brand', '=', 'masterKlein')->Paginate(9);
        }

        return view('products', compact('products'));
    }

    public function target_product(Product $product) {
        return view('product', ['product' => $product]);
    }

//    public function about() {
//        return view('about');
//    }

    public function contacts() {
        return view('contacts');
    }

    public function sendMail(Request $request) {
        dump($request->name);
        dump($request->subject);
        dump($request->message);
        dd($request->email);
    }
}
