@extends('application.application')
@section('content')
<div class="modal fade bg-white" id="templatemo_search" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="w-100 pt-1 mb-5 text-right">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="" method="get" class="modal-content modal-body border-0 p-0">
            <div class="input-group mb-2">
                <input type="text" class="form-control" id="inputModalSearch" name="q" placeholder="Search ...">
                <button type="submit" class="input-group-text bg-success text-light">
                    <i class="fa fa-fw fa-search text-white"></i>
                </button>
            </div>
        </form>
    </div>
</div>
{{----}}
<div id="template-mo-zay-hero-carousel" class="carousel slide" data-bs-ride="carousel">
    <ol class="carousel-indicators">
{{--        <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="0" class="active"></li>--}}
{{--        <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="1"></li>--}}
{{--        <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="2"></li>--}}
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img style="width: 100%" src="{{asset('assets/slider_images/1.png')}}" alt="">
        </div>
        <div class="carousel-item">
            <img style="width: 100%" src="{{asset('assets/slider_images/2.png')}}" alt="">
        </div>
        <div class="carousel-item">
            <img style="width: 100%" src="{{asset('assets/slider_images/3.png')}}" alt="">
        </div>
    </div>
    <a class="carousel-control-prev text-decoration-none w-auto ps-3" href="#template-mo-zay-hero-carousel" role="button" data-bs-slide="prev">
        <i class="fas fa-chevron-left"></i>
    </a>
    <a class="carousel-control-next text-decoration-none w-auto pe-3" href="#template-mo-zay-hero-carousel" role="button" data-bs-slide="next">
        <i class="fas fa-chevron-right"></i>
    </a>
</div>
{{----}}

<section class="container py-5">
    <div class="row text-center pt-3">
        <div class="col-lg-6 m-auto">
            <h1 class="h1">YS COMPANY</h1>
            <p>
                {{__('translate.repres_txt')}}
            </p>
        </div>
    </div>
    <div class="row">
        @foreach($tris_random as $tris)
        <div class="col-12 col-md-4 p-5 mt-3">
            <div class="welc_trs">
            <a href="{{route('target.product', $tris)}}"><img src="{{asset('assets/product_image/' . $tris->image_path)}}" ></a>
            </div>
            <h5 style="height: 70px;" class="text-center mt-3 mb-3">{{$tris->name}}</h5>
            <p class="text-center"><a href="{{route('target.product', $tris)}}" class="btn btn-success">{{__('translate.more')}}</a></p>
        </div>
        @endforeach
    </div>
</section>

<section class="bg-light">
    <div class="container py-5">
        <div class="row text-center py-3">
            <div class="col-lg-6 m-auto">
                <h1 class="h1">YS COMPANY</h1>
                <h4>
                    KRAFOR
                </h4>
            </div>
        </div>
        <div class="row">
            @foreach($crafor_random as $crafor)
            <div class="col-12 col-md-4 mb-4">
                <div class="card_bg card h-100">
                    <a href="{{route('target.product', $crafor)}}">
                        <img src="{{asset('assets/product_image/' . $crafor->image_path)}}" class="card-img-top" alt="...">
                    </a>
                    <div class="card-body">
                        <a href="{{route('target.product', $crafor)}}" class="text-decoration-none text-dark" style="font-size: 27px!important;">{{$crafor->name}}</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

<section class="container py-5">
    <div class="row text-center pt-3">
        <div class="col-lg-6 m-auto">
            <h4 class="h1">YS COMPANY</h4>
            <p>
                MasterKlein
            </p>
        </div>
    </div>
    <div class="row">
        @foreach($master_random as $master)
            <div class="col-12 col-md-4 p-5 mt-3">
                <div class="welc_trs">
                    <a href="{{route('target.product', $master)}}"><img src="{{asset('assets/product_image/' . $master->image_path)}}" ></a>
                </div>
                <h5 style="height: 70px;" class="text-center mt-3 mb-3">{{$master->name}}</h5>
                <p class="text-center"><a href="{{route('target.product', $master)}}" class="btn btn-success">{{__('translate.more')}}</a></p>
            </div>
        @endforeach
    </div>
</section>
@endsection
