@extends('application.application')
@section('content')

    <div class="modal fade bg-white" id="templatemo_search" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="w-100 pt-1 mb-5 text-right">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="" method="get" class="modal-content modal-body border-0 p-0">
                <div class="input-group mb-2">
                    <input type="text" class="form-control" id="inputModalSearch" name="q" placeholder="Search ...">
                    <button type="submit" class="input-group-text bg-success text-light">
                        <i class="fa fa-fw fa-search text-white"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>

    <section class="bg-light">
        <div class="container pb-5">
            <div class="row">
                <div class="col-lg-5 mt-5 target_img">
                    <div class="card mb-3">
                        <div style="margin-top: 50px; width: 100%; height: 270px; background-size: contain; background-position: center center; background-repeat: no-repeat; background-image: url({{asset('assets/product_image/' . $product->image_path)}})""></div>
{{--                        <img class="card-img img-fluid" src="{{asset('assets/product_image/' . $product->image_path)}}" alt="Card image cap" id="product-detail">--}}
                    </div>

                </div>
                <div class="col-lg-7 mt-5">
                    <div class="card card_target">
                        <div class="card_img"></div>
                        <div class="card-body">
                            <h1 class="h2">{{$product->name}}</h1>
                            <p class="py-2">
                            @if($product->usage_type == 'ordinary')
                                <i></i>
                            @elseif($product->usage_type == 'tassel/rolle/ballon')
                                <i class="fas fa-brush"></i>
                                <i class="fas fa-spray-can"></i>
                                <i class="fas fa-paint-roller"></i>
                            @elseif($product->usage_type == 'tassel/rolle')
                                <i class="fas fa-brush"></i>
                                <i class="fas fa-paint-roller"></i>
                                @endif
                            </p>
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <h6>{{__('translate.company')}}:</h6>
                                </li>
                                <li class="list-inline-item">
                                    <p class=""><strong class="target_product_brand">{{$product->brand}}</strong></p>
                                </li>
                            </ul>

                            <h6>{{__('translate.description')}}</h6>
                            <p>{!! html_entity_decode($product->description) !!}</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
