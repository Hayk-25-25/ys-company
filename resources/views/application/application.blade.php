@include('layouts.header')
<div id="colorlib-page">
    @include('layouts.menu')
    <div id="colorlib-main">
        @yield('content')
    </div>
</div>
@include('layouts.footer')
