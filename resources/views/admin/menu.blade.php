<div>
    <nav>
        <ul class="list-group menu-list">
            <li class="list-group-item list-group-item-dark text-white bg-dark"><a class="text-white"href="{{route('main.page')}}">{{__('translate.menu_1')}}</a></li>
            <li class="list-group-item list-group-item-dark text-white bg-dark"><a class="text-white"href="{{route('add.form')}}">{{__('translate.menu_2')}}</a></li>
            <li class="list-group-item list-group-item-dark text-white bg-dark"><a class="text-white"href="{{route('redact.item')}}">{{__('translate.menu_3')}}</a></li>
            <li class="list-group-item list-group-item-dark bg-dark">
                <form action="{{ route('logout') }}" method="POST">
                    @csrf
                    <button class="btn btn-outline-light">{{__('translate.exit')}}</button>
                </form>
            </li>
        </ul>
    </nav>
</div>
