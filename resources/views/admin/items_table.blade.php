@extends('layouts.app')

@section('content')
    <div class="bd-example">
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th>{{__('translate.admin_table_1')}}</th>
                <th scope="col">{{__('translate.admin_table_2')}}</th>
                <th scope="col">{{__('translate.admin_table_3')}}</th>
                <th scope="col">{{__('translate.admin_table_4')}}</th>
                <th scope="col">{{__('translate.admin_table_5')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
            <tr>
                <th scope="row">{{$item->id}}</th>
                <td>
                    <form action="{{route("delete.item",$item)}}" method="post">
                        @csrf
                        @method("DELETE")
                        <button type="submit" class="btn btn-outline-danger">{{__('translate.admin_table_1')}}</button>
                    </form>
                </td>
                <td>
                    <form action="{{route("update.item",$item)}}" method="get">
                        @csrf
                        @method("GET")
                        <button type="submit" class="btn btn-outline-primary">{{__('translate.admin_table_2')}}</button>
                    </form>
                </td>
                <td>{{$item->name}}</td>
                <td><img style="height: 80px" src="{{asset("assets/product_image/$item->image_path")}}" alt="image"></td>
                <td>{{$item->category}}</td>
            </tr>
            @endforeach
            </tbody>

        </table>
        <div>
            {{$items->links()}}
        </div>
    </div>
@endsection
