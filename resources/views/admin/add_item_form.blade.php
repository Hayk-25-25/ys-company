@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @if (count($errors) > 0)
                <div class="error">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li style="list-style-type: none;" class="alert alert-danger">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session()->has('success'))
                <ul>
                    <li style="list-style-type: none;" class="alert alert-success">Товар добавлен</li>
                </ul>
            @endif
        </div>
        <div class="row">
            <div>
                <form enctype="multipart/form-data" action="{{route('add.action')}}" method="post">
                    @csrf

                    <label class="form-label" for="arm_name">{{__('translate.add_1')}}</label>
                    <input class="form-control" id="name_hy" name="name_hy" type="text">

                    <label class="form-label" for="rus_name">{{__('translate.add_2')}}</label>
                    <input class="form-control" id="name_ru" name="name_ru" type="text">

                    <label class="form-label" for="price">{{__('translate.add_3')}}</label>
                    <input class="form-control" id="price" name="price" type="number">

                    <label class="form-label" for="character_arm">{{__('translate.add_4')}}</label>
                    <textarea class="form-control" name="description_hy" id="character_arm" cols="30" rows="3"></textarea>

                    <label class="form-label" for="character_ru">{{__('translate.add_5')}}</label>
                    <textarea class="form-control" name="description_ru" id="character_ru" cols="30" rows="3"></textarea>

                    <label class="form-label" for="category">{{__('translate.add_6')}}</label>
                    <select class="form-control" name="brand" id="brand">
                        <option value="krafor">Krafor</option>
                        <option value="tris">TRIS</option>
                    </select><br/>

                    <label class="form-label" for="image">{{__('translate.add_7')}}</label>
                    <input class="" id="image" name="image_path" type="file"><br />

                    <button type="submit" class="btn btn-primary">{{__('translate.add_8')}}</button>
                </form>
            </div>
        </div>
    </div>
@endsection
