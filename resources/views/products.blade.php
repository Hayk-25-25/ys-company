@extends('application.application')
@section('content')
<div class="modal fade bg-white" id="templatemo_search" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="w-100 pt-1 mb-5 text-right">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="" method="get" class="modal-content modal-body border-0 p-0">
            <div class="input-group mb-2">
                <input type="text" class="form-control" id="inputModalSearch" name="q" placeholder="Search ...">
                <button type="submit" class="input-group-text bg-success text-light">
                    <i class="fa fa-fw fa-search text-white"></i>
                </button>
            </div>
        </form>
    </div>
</div>



<div class="container py-5">
    <div class="row">

        <div class="col-lg-12">
            <ul class="collapse show list-unstyled brand_category_form pl-3">
                <form class="brand_form" method="get" action="{{route('main.products')}}">
                    @csrf
                    <li style="list-style-type: none; display: inline-block;">
                        <a class="text-decoration-none" href="#">
                            <button name="brand" class="brand_btn" value="tris" type="submit">
                                <img style="border-radius: 25px" width="120px" src="{{asset('assets/img/logo-tris.png')}}" alt="logo">
                            </button>
                        </a>
                    </li>
                    <li style="list-style-type: none; display: inline-block;">
                        <a class="text-decoration-none" href="#">
                            <button name="brand" class="brand_btn" value="krafor" type="submit">
                                <img style="border-radius: 5px" width="100px" src="{{asset('assets/img/logo-krafor.png')}}" alt="logo">
                            </button>
                        </a>
                    </li>
                    <li style="list-style-type: none; display: inline-block">
                        <a class="text-decoration-none" href="#">
                            <button name="brand" class="brand_btn" value="masterKlein" type="submit">
                                <img style="border-radius: 5px" width="100px" src="{{asset('assets/img/master.png')}}" alt="logo">
                            </button>
                        </a>
                    </li>
                </form>
            </ul>
        </div>
            <div class="row">
                @foreach($products as $product)
                        <div class="col-md-4 mb-4">
                            <a href="{{route('target.product', $product)}}">
                            <div class="card card_bg mb-4 product-wap rounded-0">
                                <div style="display: flex; justify-content: space-between; align-items: center" class="card rounded-0">
                                    <div style="width: 100%; height: 270px; background-size: contain; background-position: center center; background-repeat: no-repeat; background-image: url({{asset('assets/product_image/' . $product->image_path)}})">
                                    </div>
                                </div>
                                <div class="card-body">
                                    <a style="font-family: Arial" href="{{route('target.product', $product)}}" class="h3 text-decoration-none">{{$product->name}}</a>
                                </div>
                            </div>
                            </a>
                        </div>
                @endforeach
            </div>
            <div class="pagination_links text-decoration-none row">
                {{$products->links()}}
            </div>
        </div>
    </div>
</section>

@endsection
