
<nav class="navbar navbar-expand-lg bg-dark navbar-light" id="templatemo_nav_top">
    <div class="container text-light">
        <div class="w-100 d-flex justify-content-between">
            <div>
                <i class="icons_bigger fa fa-envelope mx-2"></i>
                <a class="icons_bigger navbar-sm-brand text-light text-decoration-none" href="mailto:info@yscompany.am">info@yscompany.am</a>
                <i class="icons_bigger fa fa-phone mx-2"></i>
                <a class="icons_bigger navbar-sm-brand text-light text-decoration-none" href="tel:011666004">(011)-66-60-04  </a>
                <a class="icons_bigger navbar-sm-brand text-light text-decoration-none" href="tel:055896666">055-89-66-66</a>
            </div>
            <div class="adapt_langs">
                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                   <a style="text-transform: capitalize; text-decoration: none" class="text-white" rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                       @if($properties['name'] == 'Russian')
                           <img width="20" src="{{asset('assets/img/rus.png')}}" alt="">
                       @else
                           <img class="arm_lang" width="20" src="{{asset('assets/img/arm.png')}}" alt="">
                       @endif
                   </a>
                @endforeach
                <a style="margin-left: 30px" class="text-light" href="https://www.facebook.com/YScompany999/" target="_blank" rel="sponsored"><i class="fab fa-facebook-f fa-sm fa-fw me-2"></i></a>
                <a class="text-decoration-none text-light" target="_blank" href="https://api.whatsapp.com/send?phone=055896666"><i style="font-size: 17px" class="fab fa-whatsapp"></i></a>
            </div>
        </div>
    </div>
</nav>
<nav class="navbar navbar-expand-lg navbar-light shadow">
    <div class="container d-flex justify-content-between align-items-center">

        <a style="z-index: 999999" class="navbar-brand text-success logo h1 align-self-center" href="{{route('main.page')}}">
            <img class="main_logo" src="{{asset('assets/img/Logo-YS4.png')}}" alt="">
            <span class="ys">Y</span><span class="ys_2">S</span>
            <span class="ys">C</span><span class="ys_2">O</span><span class="ys">M</span><span class="ys_2">P</span><span class="ys">A</span><span class="ys_2">N</span><span class="ys">Y</span>
{{--            <div class="rect"></div>--}}
        </a>

        <button class="navbar-toggler border-0" type="button" data-bs-toggle="collapse" data-bs-target="#templatemo_main_nav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="align-self-center collapse navbar-collapse flex-fill  d-lg-flex justify-content-lg-between" id="templatemo_main_nav">
            <div class="flex-fill">
                <ul class="nav navbar-nav d-flex justify-content-between mx-lg-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('main.page')}}">{{__('translate.menu_1')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('main.products')}}">{{__('translate.shop')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('main.contacts')}}">{{__('translate.contact')}}</a>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</nav>
