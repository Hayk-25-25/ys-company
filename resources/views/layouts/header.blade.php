<!DOCTYPE html>
<html lang="en">
<head>
    <title>YS COMPANY</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="{{asset('assets/img/apple-icon.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/Logo-YS4.png')}}">

    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/templatemo.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
    <link rel="stylesheet" href="{{asset('assets/css/fontawesome.min.css')}}">
</head>
<body class="body_hide">
<style>

    .body_hide {
        opacity: 0;
        transition: .9s opacity ease-in-out;
    }
    .body_visible {
        opacity: 1;
    }
</style>
<script>
    setTimeout(function(){
        document.body.classList.add('body_visible');
    }, 200);
</script>
