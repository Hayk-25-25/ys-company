<footer class="bg-dark" id="tempaltemo_footer">
    <div class="container">
        <div class="row">

            <div class="col-md-4">
                <h2 class="h2 text-success border-bottom pb-3 border-light logo">
                    <a href="{{route('main.page')}}">
                        <img style="width: 100px" src="{{asset('assets/img/Logo-YS1.png')}}" alt="">
                    </a>
                </h2>
                <ul class="list-unstyled text-light footer-link-list">
                    <li>
                        <i class="fas fa-map-marker-alt fa-fw"></i>
                        {{__('translate.addr')}}
                    </li>
                    <li style="margin-left: 1%">
                        <i class="fab fa-whatsapp"></i>
                        <a class="text-decoration-none" href="https://api.whatsapp.com/send?phone=055896666">055-89-66-66</a>
                    </li>
                    <li style="margin-left: 1%">
                        <i class="fab fa-viber"></i>
                        <a class="text-decoration-none" href="viber://chat/?number=%2B+37455896666">055-89-66-66</a>
                    </li>
                    <li style="margin-left: 1%">
                        <i class="fas fa-phone-square-alt"></i>
                        <a class="text-decoration-none" href="tel:(011) 66-60-04">(011)-66-60-04</a>
                    </li>
                    <li>
                        <i class="fa fa-envelope fa-fw"></i>
                        <a class="text-decoration-none" href="mailto:ceo@yscompany.am">ceo@yscompany.am</a>
                    </li>
                    <li>
                        <i class="fa fa-envelope fa-fw"></i>
                        <a class="text-decoration-none" href="mailto:accounting@yscompany.am">accounting@yscompany.am</a>
                    </li>
                    <li>
                        <i class="fa fa-envelope fa-fw"></i>
                        <a class="text-decoration-none" href="mailto:info@yscompany.am">info@yscompany.am</a>
                    </li>
                    <li>
                        <i class="fa fa-envelope fa-fw"></i>
                        <a class="text-decoration-none" href="mailto:marketing@yscompany.am">marketing@yscompany.am</a>
                    </li>
                </ul>
            </div>

            <div class="footer_margins col-md-4">
                <h2 class="h2 text-light border-bottom pb-3 border-light">{{__('translate.shop')}}</h2>
                <ul class="list-unstyled text-light footer-link-list">
                    <form class="brand_form" method="get" action="{{route('main.products')}}">
                        @csrf
                        <li class="text-decoration-none">
                            <a class="text-decoration-none" href="#">
                                <button name="brand" class="brand_btn ftr_btn" value="tris" type="submit">
                                    TRIS
                                </button>
                            </a>
                        </li>
                        <li class="text-decoration-none">
                            <a class="text-decoration-none" href="#">
                                <button name="brand" class="brand_btn ftr_btn" value="krafor" type="submit">
                                    KRAFOR
                                </button>
                            </a>
                        </li>
                    </form>
                </ul>
            </div>

            <div class="footer_margins col-md-4">
                <h2 class="h2 text-light border-bottom pb-3 border-light">{{__('translate.map')}}</h2>
                <ul class="list-unstyled text-light footer-link-list">
                    <li><a class="text-decoration-none" href="{{route('main.page')}}">{{__('translate.menu_1')}}</li>
{{--                    <li><a class="text-decoration-none" href="{{route('main.about')}}">{{__('translate.about')}}</li>--}}
                    <li><a class="text-decoration-none" href="{{route('main.products')}}">{{__('translate.shop')}}</a></li>
                    <li><a class="text-decoration-none" href="{{route('main.contacts')}}">{{__('translate.contact')}}</a></li>
                </ul>
            </div>

        </div>

        <div class="row text-light">
            <div class="col-12">
                <div class="w-100 my-3 border-top border-light"></div>
            </div>
            <div class="col-auto me-auto">
                <ul class="list-inline text-left footer-icons">
                    <li class="list-inline-item border border-light rounded-circle text-center">
                        <a class="text-light text-decoration-none" target="_blank" href="https://www.facebook.com/YScompany999/"><i class="fab fa-facebook-f fa-lg fa-fw"></i></a>
                    </li>
                    <li class="list-inline-item border border-light rounded-circle text-center">
                        <a class="text-light text-decoration-none" target="_blank" href="https://api.whatsapp.com/send?phone=37491288088"><i class="fab fa-whatsapp fa-lg fa-fw"></i></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="w-100 bg-black py-3">
        <div class="container">
            <div class="row pt-2">
                <div class="col-12">
                    <p class="text-left text-light">
                        Copyright &copy; 2021 YS Company |
                    </p>
                </div>
            </div>
        </div>
    </div>

</footer>

<script src="{{asset('assets/js/jquery-1.11.0.min.js')}}"></script>
<script src="{{asset('assets/js/jquery-migrate-1.2.1.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/js/templatemo.js')}}"></script>
<script src="{{asset('assets/js/custom.js')}}"></script>

</body>
</html>
