@extends('application.application')
@section('content')
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />


    <div class="modal fade bg-white" id="templatemo_search" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="w-100 pt-1 mb-5 text-right">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="" method="get" class="modal-content modal-body border-0 p-0">
                <div class="input-group mb-2">
                    <input type="text" class="form-control" id="inputModalSearch" name="q" placeholder="Search ...">
                    <button type="submit" class="input-group-text bg-success text-light">
                        <i class="fa fa-fw fa-search text-white"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>


    <!-- Start Content Page -->
    <div class="container-fluid bg-light py-5">
        <div class="col-md-6 m-auto text-center">
            <h1 class="h1">{{__('translate.contact_us')}}</h1>
            <p>
                Your text
            </p>
        </div>
    </div>

    <!-- Start Map -->
    <div id="mapid" style="width: 100%; height: 300px;"></div>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    <script>
        var mymap = L.map('mapid').setView([40.158997, 44.540869], 13);

        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
            maxZoom: 18,
            attribution: 'YS COMPANY | YS COMPANY <a href="https://templatemo.com/">YS COMPANY</a> | Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1
        }).addTo(mymap);

        L.marker([40.158997, 44.540869]).addTo(mymap)
            .bindPopup("<b>YS COMPANY</b> <br />Location.").openPopup();

        mymap.scrollWheelZoom.disable();
        mymap.touchZoom.disable();
    </script>
    <!-- Ena Map -->

    <!-- Start Contact -->
    <div class="container py-5">
        <div class="row py-5">
            <h1 class="ys">YS COMPANY</h1>
            @if (count($errors) > 0)
                <div class="error">
                    <ul>
                        <li style="list-style-type: none;" class="alert alert-danger">{{ __('translate.mess_err') }}</li>
                    </ul>
                </div>
            @endif
            @if(isset($success))
                <ul>
                    <li style="list-style-type: none;" class="alert alert-success">{{__("translate.mess_send")}}</li>
                </ul>
            @endif
            <form class="col-md-7 m-auto" action="{{route('main.send.message')}}" method="get" role="form">
                @csrf
                <div class="row">
                    <div class="form-group col-md-6 mb-3">
                        <label for="inputname">{{__('translate.name')}}</label>
                        <input type="text" class="form-control mt-1" id="name" name="name" placeholder="Name">
                    </div>
                    <div class="form-group col-md-6 mb-3">
                        <label for="inputemail">{{__('translate.email')}}</label>
                        <input type="email" class="form-control mt-1" id="email" name="email" placeholder="Email">
                    </div>
                </div>
                <div class="mb-3">
                    <label for="inputsubject">{{__('translate.subject')}}</label>
                    <input type="text" class="form-control mt-1" id="subject" name="subject" placeholder="Subject">
                </div>
                <div class="mb-3">
                    <label for="inputmessage">{{__('translate.message')}}</label>
                    <textarea class="form-control mt-1" id="message" name="message" placeholder="Message" rows="8"></textarea>
                </div>
                <div class="row">
                    <div class="col text-end mt-2">
                        <button type="submit" class="btn btn-success btn-lg px-3">{{__('translate.send')}}</button>
                    </div>
                </div>
            </form>
            <div class="col-md-5 mt-5">
                <div class="contacts">
                    <div><p style="width: 100%"><i style="font-size: 20px" class="contact_icon fas fa-home"></i>{{__('translate.addr')}}</p></div>
                    <div><p style="width: 100%"><i style="font-size: 20px" class="contact_icon fas fa-phone-square"></i> (011)-66-60-04</p></div>
                    <div><p style="width: 100%"><i style="font-size: 20px" class="contact_icon fas fa-envelope"></i> <a class="text-decoration-none text-dark" href="mailto:ceo@yscompany.am">ceo@yscompany.am</a></p></div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Contact -->

@endsection
